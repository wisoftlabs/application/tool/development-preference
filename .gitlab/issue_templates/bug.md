### Description

System OS:
Application Version:

More detailed explanatory text, if necessary. If you do not need it, delete this topic.

<details>
<summary>Please click to see the details.</summary> <br />

If you do not need it, delete this topic.   
If you have further information about the missing feature such as technical documentation or a similar feature in ano....

#### Relevant links

#### Screenshots

#### Logs
<pre><code>Paste logs here</code></pre>

</details>


### Expected Result

More detailed explanatory text, if necessary. If you do not need it, delete this topic.

<details>
<summary>Please click to see the details.</summary> <br />

If you do not need it, delete this topic.   
If you have further information about the missing feature such as technical documentation or a similar feature in ano....

#### Relevant links

#### Screenshots

</details>


### Progress

* Planning
  * [ ] Set label: In PLANNING
  * [ ] Establish a schedule
  * [ ] Adjust the epic/milestone
  * [ ] Record estimated time
* Progress 
  * [ ] Set label: In PROGRESS
  * Analysis
    * [ ] Create analytical reports(if necessary) [Analysis Report](link)
    * [ ] Record spend time
  * Design
    * [ ] Create design reports(if necessary) [Design Report](link)
    * [ ] Record spend time
  * Development 
    * An implementation list
      * [ ] Add implementation item
    * [ ] Record spend time
  * Test 
    * [ ] Check the test results
    * [ ] Record spend time
* Review & Merge Request
  * [ ] Set label: In REVIEW
  * [ ] Check commit message
  * [ ] Review the code & document
  * [ ] Merge Request
  * [ ] Record spend time
